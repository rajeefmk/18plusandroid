package Util;

/**
 * Created by rj on 6/7/15.
 */
public class Constants {

    public static final String AADHAR_NUMBER_TEST ="246366036482";
    public static final String BASE_URL = "http://18plus.io/api/";
    public static final String IS_REGISTERED = "isRegistered/";
    public static final String DO_REGISTER = "doRegister";

    public static final String AADHAR_NUMBER = "aadharnumber";
    public static final String MOBILE_NUMBER = "mobilenumber";
    public static final String EMAIL_ID = "emailid";
    public static final String PASSWORD = "password";

    public static final String ISABOVEEIGHTEEN = "age";

    //Keys for Network calls
    public static final String REGISTER_AADHAAR = "aadhaar";
    public static final String REGISTER_MOBILE = "mobile";
    public static final String REGISTER_EMAIL = "email";
    public static final String REGISTER_PASSWORD = "password";
    public static final String REGISTER_GENDER = "gender";
    public static final String REGISTER_NAME = "name";

}
