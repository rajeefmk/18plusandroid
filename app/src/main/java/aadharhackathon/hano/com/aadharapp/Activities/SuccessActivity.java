package aadharhackathon.hano.com.aadharapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import Util.Constants;
import aadharhackathon.hano.com.aadharapp.R;

public class SuccessActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        TextView message = (TextView) findViewById(R.id.successMessage);
        ImageView successImage = (ImageView) findViewById(R.id.successMessageImage);
        ImageView failureImage = (ImageView) findViewById(R.id.failureMessageImage);

        Intent mIntent = getIntent();

        if (mIntent.getBooleanExtra(Constants.ISABOVEEIGHTEEN, false)) {

            successImage.setVisibility(View.VISIBLE);
            message.setText(getResources().getString(R.string.textview_success_message));
            runThread();
        } else {

            failureImage.setVisibility(View.VISIBLE);
            message.setText(getResources().getString(R.string.textview_failure_message));

        }

    }

    private void runThread() {

        Thread background = new Thread(){

            public void run() {

                try{

                    sleep(3*1000);
                    Intent mIntent = new Intent(getApplicationContext(), LoginActivity.class );
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mIntent);
                    finish();

                }catch(Exception e){

                }
            }
        };
        background.start();
    }

    private void checkForCrashes() {
        CrashManager.register(this, "678ef956e2257f26929f724c31c45485");
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this, "678ef956e2257f26929f724c31c45485");
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
        checkForUpdates();
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_success, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
