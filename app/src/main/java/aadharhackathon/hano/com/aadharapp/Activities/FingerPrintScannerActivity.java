package aadharhackathon.hano.com.aadharapp.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aadhaarconnect.bridge.capture.model.auth.AuthCaptureData;
import com.aadhaarconnect.bridge.capture.model.auth.AuthCaptureRequest;
import com.aadhaarconnect.bridge.capture.model.common.ConsentType;
import com.aadhaarconnect.bridge.capture.model.common.Location;
import com.aadhaarconnect.bridge.capture.model.common.LocationType;
import com.aadhaarconnect.bridge.capture.model.common.request.CertificateType;
import com.aadhaarconnect.bridge.capture.model.common.request.Modality;
import com.aadhaarconnect.bridge.capture.model.common.request.ModalityType;
import com.aadhaarconnect.bridge.capture.model.kyc.KycCaptureData;
import com.aadhaarconnect.bridge.util.AadhaarBridgeUtil;
import com.google.protobuf.ByteString;
import com.morpho.capture.AuthBfdCap;
import com.morpho.capture.MorphoTabletFPSensorDevice;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import java.io.IOException;
import java.text.ParseException;

import Util.Constants;
import aadharhackathon.hano.com.aadharapp.R;
import in.gov.uidai.authserver.protobuf.Auth;

public class FingerPrintScannerActivity extends ActionBarActivity implements AuthBfdCap {

    MorphoTabletFPSensorDevice fpSensorDevice;
    ImageView imageView;
    byte[] data = null;
    private static final String BASE_URL="https://ac.khoslalabs.com/hackgate/hackathon";
    private static final String TEST_URL = "http://requestb.in/nu7r9unu";
    TextView headerText;
    TextView statusText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_print_scanner);

        imageView = (ImageView) findViewById(R.id.fingerPrintImage);
        fpSensorDevice = new MorphoTabletFPSensorDevice(this);
        fpSensorDevice.open(this);

        headerText = (TextView) findViewById(R.id.headerText);
        headerText.setVisibility(View.GONE);

        statusText = (TextView) findViewById(R.id.statusText);
        statusText.setText("Please click capture to start scanning");

    }

    @Override
    public void updateImageView(final ImageView img, final Bitmap bitmap, final String msg, boolean b, int i) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (img != null) {
                    img.setImageBitmap(bitmap);
                }
                if (msg.equalsIgnoreCase("Finger captured successfully")) {
                    try {

                        Toast.makeText(getApplicationContext(),
                                "Finger captured successfully",
                                Toast.LENGTH_SHORT).show();

                        data = fpSensorDevice.templateBuffer;

                        Intent mIntent = getIntent();
                        String aadharNumber = mIntent.getStringExtra(Constants.AADHAR_NUMBER);

                        //KycCaptureData kycData = buildKycRequest("265600416139", data);
                        KycCaptureData kycData = buildKycRequest(aadharNumber,data);
                        runAsyncTask(kycData);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    Log.i("finger", msg);
                    statusText.setText("Please try again.");

                }

            }
        });


    }

    private void runAuthAsyncTask(AuthCaptureData authData) {

        AadhaarAuthAsyncTskAuth authAsyncTskAuth = new AadhaarAuthAsyncTskAuth(this, authData);
        authAsyncTskAuth.execute(BASE_URL + "/auth");
        //authAsyncTskAuth.execute(TEST_URL);
    }

    private void runAsyncTask(KycCaptureData kycData) {

        AadhaarAuthAsyncTskKYC authAsyncTask = new AadhaarAuthAsyncTskKYC(this,
                kycData);
       // authAsyncTask.execute(TEST_URL);
        authAsyncTask.execute(BASE_URL + "/kyc");

    }

    private AuthCaptureData buildAuthRequest(String aadharNumber, byte[] data) throws IOException, ParseException {

        AuthCaptureRequest authCaptureRequest = new AuthCaptureRequest();
        authCaptureRequest.setAadhaar(aadharNumber);
        authCaptureRequest.setModality(Modality.biometric);
        authCaptureRequest.setModalityType(ModalityType.fp);
        authCaptureRequest.setNumOffCapture(1);
        authCaptureRequest.setCertificateType(CertificateType.preprod);

        Auth.Bios fp = null;


         /** FOR BIOMETRIC AUTHENTICATION. CAPTURE THE FP FMR DATA FROM THE DEVICE.
		 * SAMPLE HERE IS READING THE DATA FROM FILE WHICH NEEDS TO BE MODIFIED
		 **/


         fp = Auth.Bios.newBuilder().addBio(Auth.Bio.newBuilder()
                .setType(Auth.BioType.FMR)
                .setPosh(Auth.Position.RIGHT_INDEX)
                .setContent(ByteString.copyFrom(data)))
                .build();

        Location loc = new Location();
        loc.setType(LocationType.pincode);

		 //SET THE LOCATION OF THE DEVICE
        loc.setPincode("560025");
        authCaptureRequest.setLocation(loc);

        AuthCaptureData authCaptureData = AadhaarBridgeUtil.buildAuthRequest(authCaptureRequest,fp);

        return authCaptureData;

    }
    private KycCaptureData buildKycRequest(String aadharNumber, byte[] fingerprintImage) throws IOException, ParseException {

        AuthCaptureRequest authCaptureRequest = new AuthCaptureRequest();
        authCaptureRequest.setAadhaar(aadharNumber);
        authCaptureRequest.setModality(Modality.biometric);
        authCaptureRequest.setModalityType(ModalityType.fp);
        authCaptureRequest.setNumOffCapture(1);
        authCaptureRequest.setCertificateType(CertificateType.preprod);

        Auth.Bios fp = null;

		/*
         * FOR BIOMETRIC AUTHENTICATION. CAPTURE THE FP FMR DATA FROM THE DEVICE.
		 * SAMPLE HERE IS READING THE DATA FROM FILE WHICH NEEDS TO BE MODIFIED
		 *
		 */

       fp=  Auth.Bios.newBuilder().addBio(Auth.Bio.newBuilder()
                .setType(Auth.BioType.FMR)
                .setPosh(Auth.Position.RIGHT_INDEX)
                .setContent(ByteString.copyFrom(fingerprintImage)))
                .build();

        Location loc = new Location();
        loc.setType(LocationType.pincode);

		/* SET THE LOCATION OF THE DEVICE */
        loc.setPincode("560025");
        authCaptureRequest.setLocation(loc);
        KycCaptureData kycCaptureData
                = AadhaarBridgeUtil.buildKycRequest(authCaptureRequest, fp, ConsentType.Y);

        return kycCaptureData;

    }

    @Override
    public void setQlyFinger(int i) {

    }

    private boolean isDeviceConnected() {
        boolean deviceStatus = fpSensorDevice.isDeviceConnected();

        if (!deviceStatus)
            Toast.makeText(getApplicationContext(),
                    "deviceStatus:  " + deviceStatus,
                    Toast.LENGTH_SHORT).show();
        return deviceStatus;
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
        checkForUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseScanner();
        
    }

    public void startCapture(View v) {

        headerText.setVisibility(View.VISIBLE);

        if(isDeviceConnected()) {

            fpSensorDevice.setViewToUpdate(imageView);
            try {

                fpSensorDevice.startCapture();
               // fpSensorDevice.ge
            }catch(Exception e){

                e.printStackTrace();
            }

        }

    }

    private void releaseScanner() {

        if (fpSensorDevice != null) {
            fpSensorDevice.cancelLiveAcquisition();
            fpSensorDevice.release();
            fpSensorDevice = null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        fpSensorDevice = new MorphoTabletFPSensorDevice(this);
        fpSensorDevice.open(this);
    }

    private void checkForCrashes() {
        CrashManager.register(this, "678ef956e2257f26929f724c31c45485");
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this, "678ef956e2257f26929f724c31c45485");
    }


}
